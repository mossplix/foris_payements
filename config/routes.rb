Sservice::Application.routes.draw do
  root :to => 'sservice#index', :as => 'home'
  match 'transact', :to => 'sservice#transact', :as => 'transact'
end
