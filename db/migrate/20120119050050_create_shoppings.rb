class CreateShoppings < ActiveRecord::Migration
  def change
    create_table :shoppings do |t|
      t.text        :number
      t.integer     :bundle_id
      t.timestamps
    end
  end
end
