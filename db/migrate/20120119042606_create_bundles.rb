class CreateBundles < ActiveRecord::Migration
  def change
    create_table :bundles do |t|
      t.text        :name
      t.text        :descr
      t.float       :amount
      t.timestamps
    end
  end
end
