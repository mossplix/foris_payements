if ENV['DEMO_CLIENT'] == 'orange' then
  Bundle.create([
    {:name => 'All Day Longer', :amount => 10_000, :descr => '100MBps speeds for 24 hours.'},
    {:name => 'All Week Longer', :amount => 60_000, :descr => '100MBps speeds for 7 days.'},
    {:name => 'Monthmatics', :amount => 240_000, :descr => 'Unlimited speeds for a month.'},
    {:name => 'Freehold Internet', :amount => 1_200_000, :descr => 'A yearly lease of unlimited speeds.'}
  ])
else
  Bundle.create([
    {:name  => '500 MB', :amount => 15_000, :descr => 'Valid for 10 days.'},
    {:name  => '1GB',    :amount => 30_000, :descr => ''},
    {:name  => '2GB',    :amount => 45_000, :descr => ''},
    {:name  => '4GB',    :amount => 60_000, :descr => ''},
    {:name  => '6GB',    :amount => 85_000, :descr => ''}
  ])
end
