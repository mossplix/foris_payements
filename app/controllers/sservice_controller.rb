class SserviceController < ApplicationController
  def index
    if request[:next] then
      session[:next] = request[:next]
    end
    @bundles = Bundle.order('amount')
    if session[:phonenumber] then
      @shoppings = Shopping.order('created_at DESC').where(['number = ?', session['phonenumber']])
    end
  end

  def transact
    if request[:phonenumber] then
      session[:phonenumber] = request[:phonenumber]
      if request[:bundle] then
        @bundle = Bundle.find_by_id request[:bundle]
        session[:bundle] = @bundle.id
        cpt = Sinapi::Account.new session[:phonenumber]
        ans = cpt.pay(:to => '256718202639', :amount => @bundle.amount, :currency => 'UGX', :test => :test, :comment => (%[%s: %s] % [@bundle.name, @bundle.descr]), :from => session[:phonenumber], :username => :scyfy, :password => :halpulaar)
        if ans.is_a? String then
          flash[:error] = ans
          redirect_to home_path
        else
          Shopping.create :bundle_id => @bundle.id, :number => session[:phonenumber]
          if session[:next] then
            session.delete :next
            redirect_to session[:next]
          else
            @balance = cpt.balance :username => :scyfy, :password => :halpulaar
          end
        end
      else
        flash[:error] = %[Choose a bundle.]
        redirect_to home_path
      end
    else
      flash[:error] = %[Enter a phone number.]
      redirect_to home_path
    end
  end
end
