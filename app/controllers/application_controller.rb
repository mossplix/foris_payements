class ApplicationController < ActionController::Base
  protect_from_forgery
  before_filter :list_shoppings

  def list_shoppings
    if session[:phonenumber] then
      @shoppings = Shopping.order('created_at DESC').where(['number = ?', session['phonenumber']])
    else
      @shoppings = []
    end
  end
end
