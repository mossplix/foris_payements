class Developer < ActiveRecord::Base
  def self.locate request
    dev = self.find_by_username request[:username]
    if dev then
      sha = Digest::SHA1.new << %[#{dev.sha1_salt}#{request[:password]}]
      if sha.to_s == dev.sha1_pass then
        dev
      else
        nil
      end
    else
      nil
    end
  end
end
