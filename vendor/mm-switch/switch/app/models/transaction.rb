class Transaction < ActiveRecord::Base
  scope :as_latest, order('created_at DESC')

  def self.execute! request
    tx    = Transaction.create request
    from  = Account.find_by_identifier request[:from]
    unless from then
      from = Account.create :identifier => request[:from], :currency => 'USD', :balance => 0.0, :available_balance => 0.0
    end
    to    = Account.find_by_identifier request[:to]
    unless to then
      to = Account.create :identifier => request[:to], :currency => 'USD', :balance => 0.0, :available_balance => 0.0
    end
    tx.to_prior_balance   = to.balance
    tx.from_prior_balance = from.balance
    tx.to_prior_available_balance   = to.available_balance
    tx.from_prior_available_balance = from.available_balance
    amnt  = request[:amount].to_f * Currency.find_by_code(request[:currency]).gld_ounces
    if from.available_balance_in_gold >= amnt then
      to.balance    = to.balance   + (amnt / to.curr.gld_ounces)
      from.balance  = from.balance - (amnt / from.curr.gld_ounces)
      from.available_balance  = from.available_balance - (amnt / from.curr.gld_ounces)
      to.save
      from.save
      tx.succeeded = true
      tx.message = %[Successful.]
    else
      tx.succeeded = false
      tx.message = %[#{request[:from]} has insufficient available balance]
    end
    tx.to_latter_balance   = to.balance
    tx.from_latter_balance = from.balance
    tx.to_latter_available_balance   = to.available_balance
    tx.from_latter_available_balance = from.available_balance
    tx.save
    tx
  end

  def curr
    Currency.find_by_code(self.currency)
  end
end
