class User < ActiveRecord::Base
  has_many :user_actions

  def self.to session, reason
    ua = UserAction.create(:description => reason, :user_id => session[:user_id])
    t1 = Time.now
    if block_given? then
      begin
        yield self
        ua.time_spent = Time.now - t1
        ua.successful = true
        ua.save
      rescue Exception => e
        ua.time_spent = Time.now - t1
        ua.successful = false
        ua.message    = e.message
        ua.save
        raise e
      end
    end
  end
end
