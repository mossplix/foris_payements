class Account < ActiveRecord::Base
  def curr
    Currency.find_by_code self.currency
  end

  def available_balance_in_gold
    self.available_balance * Currency.find_by_code(self.currency).gld_ounces
  end

  def balance_in_gold
    self.balance * Currency.find_by_code(self.currency).gld_ounces
  end
end
