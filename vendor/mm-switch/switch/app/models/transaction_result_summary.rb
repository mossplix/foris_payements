class TransactionResultSummary < ActiveRecord::Base
  def initialize successful, trx
    @successful   = successful
    @transactions = trx
    super()
  end

  def self.both trx
    [true, false].map {|x| TransactionResultSummary.new(x, trx)}
  end

  def quantity
    if @successful then
      @transactions.where('succeeded').count
    else
      @transactions.where('NOT succeeded').count
    end
  end

  def name
    if @successful then
      'Successful'
    else
      'Failed'
    end
  end
end
