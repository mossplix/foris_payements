class SwitchController < ApplicationController
  respond_to :json, :html

  before_filter :ensure_logged_in, :except => [:auth]
  before_filter :start_timer, :only => [:transact, :balance, :classify, :forex]
  after_filter  :end_timer, :only => [:transact, :balance, :classify, :forex]

  def limit_to_period models
    ans = models
    ans = models.where(['EXTRACT(YEAR FROM created_at) = ?', request[:year]]) if request[:year]
    ans = ans.where(['EXTRACT(MONTH FROM created_at) = ?', request[:month]]) if request[:month]
    ans = ans.where(['EXTRACT(DAY FROM created_at) = ?', request[:day]]) if request[:day]
    ans = ans.where(['EXTRACT(HOUR FROM created_at) = ?', request[:hour]]) if request[:hour]
    ans = ans.where(['EXTRACT(MINUTE FROM created_at) = ?', request[:minute]]) if request[:minute]
    ans
  end

  def index
    @transactions = limit_to_period(Transaction.order('created_at DESC').paginate(:page => request[:page]))
  end

  def stats
    @transactions = limit_to_period(Transaction.where('TRUE'))
    @summaries    = TransactionResultSummary.both(@transactions)
    @timing       = limit_to_period(Timing.select('AVG(seconds_spent) AS seconds_spent')).first
    @txsets       = @transactions.inject({}) do |p, n|
      cle = n.created_at.localtime.strftime('%Y/%m/%d/%H/%M')
      p[cle] ||= [0, 0]
      p[cle][0] = p[cle][0] + 1
      p[cle][1] = p[cle][1] + ((Currency.find_by_code(n.currency).gld_ounces * n.amount) / Currency.find_by_code('USD').gld_ounces)
      p
    end
  end

  def statement
    @transactions = limit_to_period(Transaction.where(['"from" = ? OR "to" = ?', request[:number], request[:number]]).paginate(:page => request[:page]).as_latest)
    render 'index'
  end

  def accounts
    @accounts = Account.order('identifier ASC').paginate :page => request[:page]
  end

  def admin
    @acts = limit_to_period(UserAction.where(['user_id = ?', session[:user_id]]).order('created_at DESC').paginate(:page => request[:page]))
    render 'admins'
  end

  def admins
    @acts = limit_to_period(UserAction.where('user_id IS NOT NULL').order('created_at DESC').paginate(:page => request[:page]))
  end

  def simulate
    tx = nil
    if request[:from] then
      User.to session, %[Simulate a transaction] do
        tx = Transaction.execute! :to => request[:to], :from => request[:from], :amount => request[:amount], :currency => request[:currency], :comment => request[:comment]
        @error = tx.message unless tx.succeeded
      end
    end
    @currencies = Currency.order('gld_ounces DESC')
    if tx then
      respond_with({:status => tx.succeeded, :message => tx.message})
    else
      respond_with({:status => false, :message => %[Bad request.]})
    end
  end

  def balance
    compte = Account.find_by_identifier request[:account]
    ans =
    unless compte then
      {:status => false, :message => %[Account ] + (request[:account].nil? ? 'not indicated.' : %[(#{request[:account].inspect}) not known.])}
    else
      {:status => true, :available => compte.available_balance.round(3), :actual => compte.balance.round(3), :currency => compte.currency}
    end
    respond_with ans
  end

  def transact
    if request[:test] == 'test' then
      simulate
    else
      # TODO: We will do this after a deal is signed.
      respond_with({:status => false, :message => %[Currently not connected to any m-Banking network.]})
    end
  end

  def forex
    one = Currency.find_by_code request[:one]
    ans = unless one then
      {:status => false, :message => %[Unknown currency: #{request[:one].inspect}]}
    else
      dest = Currency.find_by_code request[:in]
      if dest then
        {:status => true, :rate => (one.gld_ounces / dest.gld_ounces).round(3)}
      else
        {:status => false, :message => %[Unknown currency: #{request[:in].inspect}]}
      end
    end
    respond_with ans
  end

  def classify
    respond_with({:status => true, :number => request[:number], :network => %[Sinapi]})
  end

  def badges
  end

  def logout
    User.to session, %[Log out]
    session.delete :user_id
    redirect_to auth_path
  end

  def auth
    return redirect_to(home_path) if session[:user_id]
    if request[:user] then
      user = User.find_by_id request[:user]
      if user then
        user.last_attempt = Time.now
        user.save
        sha  = Digest::SHA1.new << %[#{user.sha1_salt}#{request[:password]}]
        if sha.to_s == user.sha1_pass then
          session[:user_id] = user.id
          User.to session, %[Log in]
          return redirect_to(home_path)
        else
          @error = %[Access denied.]
        end
      else
        @error = %[Access denied.]
      end
    end
    @users = User.order 'name ASC'
  end

  def ensure_logged_in
    if request[:format] then
      unless Developer.locate request then
        respond_with({:status => false, :message => %[Access denied.]})
      end
    else
      unless session[:user_id] then
        redirect_to :auth, :return => request.path
      end
    end
  end

  def start_timer
    if request.format == 'json' then
      @started = Time.now
    else
      skip_after_filter :end_timer
    end
  end

  def end_timer
    Timing.create :action => request[:action], :developer_username => request[:username], :seconds_spent => Time.now - @started
  end
end
