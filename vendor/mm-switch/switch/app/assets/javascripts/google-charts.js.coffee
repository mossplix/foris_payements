google.load 'visualization', '1.0', {'packages':['corechart'
                                                 'gauge']}
google.setOnLoadCallback(->
  if document.getElementById('visualisations')
    data = new google.visualization.DataTable()
    data.addColumn 'string', 'End State'
    data.addColumn 'number', 'Total'
    for row in document.getElementsByClassName 'sfrow'
      tds = row.getElementsByTagName('td')
      data.addRows([[tds[0].innerHTML.trim(), parseInt(tds[1].innerHTML.trim())]])
    pie = new google.visualization.PieChart(document.getElementById('successfulfailed'))
    pie.draw data, {title:'Transaction End State (Successful versus Failed)', width:500, height:500, is3D:true}
    
    response = new google.visualization.DataTable()
    response.addColumn 'number', 'Responsivity'
    response.addRows [
      [parseFloat(document.getElementById('responsetimes').getElementsByClassName('data')[0].innerHTML.trim())]
    ]
    gauge = new google.visualization.Gauge(document.getElementById('responsetimes'))
    gauge.draw response, {title:'Response Times', width:300, height:300, is3D:true, redFrom: 81, redTo:120, yellowFrom:41, yellowTo:80, minorTicks:1}

    txs1 = new google.visualization.DataTable()
    txs2 = new google.visualization.DataTable()
    txs1.addColumn 'datetime', 'Time'
    txs1.addColumn 'number', 'Amounts Transacted ($)'
    txs2.addColumn 'datetime', 'Time'
    txs2.addColumn 'number', 'Number of Transactions'
    for row in document.getElementsByClassName('txrow')
      tds = row.getElementsByTagName('td')
      datePieces = tds[0].innerHTML.trim().split('/')
      txs1.addRows [
        # alert(new Date(datePieces[0], datePieces[1], datePieces[2], datePieces[3]).toString() + ' from [' + datePieces.toString() + '] of ' + tds[0].innerHTML.trim())
        [new Date(datePieces[0], parseInt(datePieces[1]) - 1, datePieces[2], datePieces[3], datePieces[4]), parseFloat(tds[2].innerHTML.trim())]
      ]
      txs2.addRows [
        [new Date(datePieces[0], parseInt(datePieces[1]) - 1, datePieces[2], datePieces[3], datePieces[4]), parseFloat(tds[1].innerHTML.trim())]
      ]
    walker = new google.visualization.AreaChart(document.getElementById('txssummary'))
    # walker = new PilesOfMoney(document.getElementById('txssummary'))
    walker.draw txs1, {title:'Transaction Volume', width:800, height:500, is3D:true, vAxis: {title:'Amounts'}, hAxis:{title:'Time', format:'2010/03/01'}}

    hits = new google.visualization.LineChart(document.getElementById('txhits'))
    hits.draw txs2, {title:'Number of Transactions', width:800, height:500, is3D:true, vAxis: {title:'Hits'}, hAxis:{title:'Time', format:'2010/03/01'}}
)
