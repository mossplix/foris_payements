# encoding: UTF-8

root = User.create(:name => 'Revence Kalibwani', :sha1_pass => %[75260094ada09d7602a9e67ebebe0baf0d7d3033], :sha1_salt => %[demgalam])
dummy = User.create(:name => 'Francis Otim', :sha1_pass => %[a5b651cf046be5441b6c7cd5363ac1504c180b7f], :sha1_salt => %[pulho])

currencies = Currency.create([
  {
    :name => 'US Dollar',
    :symbol => '$',
    :code => 'USD',
    :gld_ounces => (1.0 / 1_500.0)
  },
  {
    :name => 'British Pound Sterling',
    :symbol => '£',
    :code => 'GBP',
    :gld_ounces => (1.0 / 7_00.0)
  },
  {
    :name => 'Uganda Shilling',
    :symbol => 'Shs',
    :code => 'UGX',
    :gld_ounces => (1.0 / (1_500.0 * 2_500))
  },
  {
    :name => 'Gold',
    :symbol => 'oz.',
    :code => 'GLD',
    :gld_ounces => 1.0
  }
])

me = Account.create :identifier => '256772344681', :balance => 30_000_000_000.0,                     :currency => 'GLD', :available_balance => 7_000_000.0
ut = Account.create :identifier => '256718202639', :balance => 1_200_000.0,                             :currency => 'UGX', :available_balance => 15_000.0

Developer.create :username => 'scyfy', :sha1_pass => %[51d9120bcf7a426d483bda6b30389aea31af867e], :sha1_salt => 'kowoni'
