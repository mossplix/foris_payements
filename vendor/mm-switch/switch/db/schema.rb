# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended to check this file into your version control system.

ActiveRecord::Schema.define(:version => 20111208021020) do

  create_table "accounts", :force => true do |t|
    t.text     "identifier",        :null => false
    t.float    "balance",           :null => false
    t.float    "available_balance", :null => false
    t.text     "currency",          :null => false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "currencies", :force => true do |t|
    t.text     "name"
    t.text     "code"
    t.text     "symbol"
    t.float    "gld_ounces"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "developers", :force => true do |t|
    t.text     "username"
    t.text     "sha1_pass"
    t.text     "sha1_salt"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "timings", :force => true do |t|
    t.float    "seconds_spent"
    t.text     "action"
    t.text     "developer_username"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "transaction_result_summaries", :force => true do |t|
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "transactions", :force => true do |t|
    t.text     "to",                                                       :null => false
    t.text     "from",                                                     :null => false
    t.float    "amount",                                                   :null => false
    t.text     "currency",                                                 :null => false
    t.text     "comment",                                                  :null => false
    t.boolean  "succeeded",                     :default => false,         :null => false
    t.text     "message",                       :default => "Incomplete.", :null => false
    t.boolean  "fraudulent",                    :default => false,         :null => false
    t.float    "to_prior_balance"
    t.float    "from_prior_balance"
    t.float    "to_latter_balance"
    t.float    "from_latter_balance"
    t.float    "to_prior_available_balance"
    t.float    "from_prior_available_balance"
    t.float    "to_latter_available_balance"
    t.float    "from_latter_available_balance"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "user_actions", :force => true do |t|
    t.text     "description"
    t.integer  "user_id"
    t.boolean  "successful",  :default => true
    t.text     "message"
    t.float    "time_spent",  :default => 0.0
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "users", :force => true do |t|
    t.text     "name"
    t.text     "sha1_pass"
    t.text     "sha1_salt"
    t.datetime "last_attempt"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

end
