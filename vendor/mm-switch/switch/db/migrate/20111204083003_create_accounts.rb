class CreateAccounts < ActiveRecord::Migration
  def change
    create_table :accounts do |t|
      t.text        :identifier, :null => false
      t.float       :balance, :null => false
      t.float       :available_balance, :null => false
      t.text        :currency, :null => false
      t.timestamps
    end
  end
end
