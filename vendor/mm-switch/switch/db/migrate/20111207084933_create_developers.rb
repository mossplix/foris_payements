class CreateDevelopers < ActiveRecord::Migration
  def change
    create_table :developers do |t|
      t.text        :username
      t.text        :sha1_pass
      t.text        :sha1_salt
      t.timestamps
    end
  end
end
