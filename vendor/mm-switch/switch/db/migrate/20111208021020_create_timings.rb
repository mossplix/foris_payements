class CreateTimings < ActiveRecord::Migration
  def change
    create_table :timings do |t|
      t.float       :seconds_spent
      t.text        :action
      t.text        :developer_username
      t.timestamps
    end
  end
end
