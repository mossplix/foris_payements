class CreateUsers < ActiveRecord::Migration
  def change
    create_table :users do |t|
      t.text            :name
      t.text            :sha1_pass
      t.text            :sha1_salt
      t.timestamp       :last_attempt
      t.timestamps
    end
  end
end
