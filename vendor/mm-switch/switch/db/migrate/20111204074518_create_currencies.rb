class CreateCurrencies < ActiveRecord::Migration
  def change
    create_table :currencies do |t|
      t.text        :name
      t.text        :code
      t.text        :symbol
      t.float       :gld_ounces
      t.timestamps
    end
  end
end
