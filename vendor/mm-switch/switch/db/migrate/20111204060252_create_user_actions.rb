class CreateUserActions < ActiveRecord::Migration
  def change
    create_table :user_actions do |t|
      t.text          :description
      t.integer       :user_id
      t.boolean       :successful, :default => true
      t.text          :message
      t.float         :time_spent, :default => 0.0
      t.timestamps
    end
  end
end
