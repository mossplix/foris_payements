class CreateTransactionResultSummaries < ActiveRecord::Migration
  def change
    create_table :transaction_result_summaries do |t|

      t.timestamps
    end
  end
end
