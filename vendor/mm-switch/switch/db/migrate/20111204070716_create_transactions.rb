class CreateTransactions < ActiveRecord::Migration
  def change
    create_table :transactions do |t|
      t.text          :to,         :null => false
      t.text          :from,       :null => false
      t.float         :amount,     :null => false
      t.text          :currency,   :null => false
      t.text          :comment,    :null => false
      t.boolean       :succeeded,  :null => false, :default => false
      t.text          :message,    :null => false, :default => 'Incomplete.'
      t.boolean       :fraudulent, :null => false, :default => false
      t.float         :to_prior_balance
      t.float         :from_prior_balance
      t.float         :to_latter_balance
      t.float         :from_latter_balance
      t.float         :to_prior_available_balance
      t.float         :from_prior_available_balance
      t.float         :to_latter_available_balance
      t.float         :from_latter_available_balance
      t.timestamps
    end
  end
end
