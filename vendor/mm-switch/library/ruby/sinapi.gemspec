Gem::Specification.new do |s|
  s.name        = 'sinapi'
  s.version     = '0.9.1'
  s.date        = '2011-12-08'
  s.summary     = "The Sinapi m-Banking Switch API"
  s.description = "An interface to the developer API of the Sinapi m-Banking Switch."
  s.authors     = ["Revence Kalibwani"]
  s.email       = 'revence@1st.ug'
  s.files       = ["lib/sinapi.rb"]
  s.homepage    =
    'http://sinapi.1st.ug'
end
