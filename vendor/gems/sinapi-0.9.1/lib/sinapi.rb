require 'net/http'
require 'json'

module Sinapi
  # SINAPI_ADDRESS = %[http://sinapi.1st.ug]
        # TODO:
  SINAPI_ADDRESS = %[http://localhost:3001]

  class Balance
    attr_reader :available, :actual, :currency
    def initialize details
      @available = details['available']
      @actual    = details['actual']
      @currency  = details['currency']
    end

    def self.for account, args
      got = JSON.parse(Net::HTTP.get(URI.parse(%[%s/balance/%s.json?username=%s&password=%s&test=%s] % ([SINAPI_ADDRESS, account] + [:username, :password, :test].map {|x| URI.escape(args[x].to_s)}))))
      if got['status'] then
        self.new got
      else
        got['message']
      end
    end
  end

  class Transaction
    def initialize args
      @args = args
    end

    def execute!
      JSON.parse(Net::HTTP.get(URI.parse(%[%s/transact.json?from=%s&to=%s&amount=%s&currency=%s&comment=%s&username=%s&password=%s&test=%s] % ([SINAPI_ADDRESS] + [:from, :to, :amount, :currency, :comment, :username, :password, :test].map {|x| URI.escape(@args[x].to_s)}))))
    end

    def self.run args
      tx  = self.new args
      ans = tx.execute!
      if ans['status'] then
        ans
      else
        ans['message']
      end
    end
  end

  class Forex
    def self.rate args
      got = JSON.parse(Net::HTTP.get(URI.parse(%[%s/forex.json?one=%s&in=%s&username=%s&password=%s&test=%s] % ([SINAPI_ADDRESS] + [:one, :in, :username, :password, :test].map {|x| URI.escape(args[x].to_s)}))))
      if got['status'] then
        got['rate']
      else
        got['message']
      end
    end
  end

  class Account
    def initialize account
      @account = account
    end

    def balance args
      Balance.for @account, args
    end

    def pay args
      args[:from] = @account
      Transaction.run args
    end

    def classify args
      got = JSON.parse(Net::HTTP.get(URI.parse(%[%s/classify/%s.json?username=%s&password=%s&test=%s] % ([SINAPI_ADDRESS, @account] + [:username, :password, :test].map {|x| URI.parse(args[x].to_s)}))))
      if got['status'] then
        got
      else
        nil
      end
    end
  end
end
